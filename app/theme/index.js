import { extendTheme } from "@chakra-ui/react";

const colors = {
  primary: "teal",
};

export const theme = extendTheme({ colors });
