import { Link } from "@remix-run/react";

export const PageLink = ({ to, children, style }) => {
  return (
    <Link
      to={to}
      style={{
        color: "blue",
        border: "2px solid blue",
        padding: "0.6rem 1rem",
        borderRadius: "0.5rem",
        display: "inline-block",
        ...style,
      }}
    >
      {children}
    </Link>
  );
};
