// external Components
import { FormLabel, Text, Box } from "@chakra-ui/react";

export const FieldWrapper = ({ children, htmlFor, error, label }) => {
  return (
    <Box
      sx={{
        mt: "1.2rem",
      }}
    >
      <FormLabel htmlFor={htmlFor}>{label}</FormLabel>
      {children}
      <ErrorMessage text={error} />
    </Box>
  );
};

const ErrorMessage = ({ text }) => {
  if (!text) return null;
  return (
    <Text
      sx={{
        color: "red",
      }}
    >
      {text}
    </Text>
  );
};
