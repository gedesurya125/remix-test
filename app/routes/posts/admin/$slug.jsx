import { json, redirect } from "@remix-run/node";
import { useLoaderData } from "@remix-run/react";
import { getPost, updatePost, deletePost } from "~/models/post.server";

// External Components
import { PostForm, PageLink } from "~/components";

export const loader = async ({ params }) => {
  const post = await getPost(params.slug);
  return json({ post });
};

export const action = async ({ request }) => {
  const formData = await request.formData();

  const title = formData.get("title");
  const slug = formData.get("slug");
  const markdown = formData.get("markdown");
  const intent = formData.get("intent");

  console.log("this is the slug", slug);

  // Validation
  const errors = {
    title: title ? null : "Title is required",
    slug: slug ? null : "Slug is required",
    markdown: markdown ? null : "Markdown is required",
  };
  const hasErrors = Object.values(errors).some((errorMessage) => errorMessage);
  if (hasErrors) {
    return json(errors);
  }

  if (intent === "delete") {
    await deletePost(slug);
  } else {
    await updatePost({
      title,
      slug,
      markdown,
    });
  }

  return redirect("/posts/admin");
};

export default function PostSlug() {
  const { post } = useLoaderData();
  console.log("this is the post", post);
  return (
    <main>
      <PageLink
        style={{
          color: "red",
          borderColor: "red",
        }}
        to="/posts/admin"
      >
        Back
      </PageLink>
      <PostForm initialValue={post} />
    </main>
  );
}
