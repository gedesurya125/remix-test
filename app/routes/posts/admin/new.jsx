import { redirect, json } from "@remix-run/node";
import { createPost } from "~/models/post.server";

// Local Components
import { PostForm, PageLink } from "~/components";

export const action = async ({ request }) => {
  const formData = await request.formData();

  const title = formData.get("title");
  const slug = formData.get("slug");
  const markdown = formData.get("markdown");

  // Validation
  const errors = {
    title: title ? null : "Title is required",
    slug: slug ? null : "Slug is required",
    markdown: markdown ? null : "Markdown is required",
  };
  const hasErrors = Object.values(errors).some((errorMessage) => errorMessage);
  if (hasErrors) {
    return json(errors);
  }

  await createPost({ title, slug, markdown });

  return redirect("/posts/admin");
};

export default function NewPost() {
  return (
    <>
      <PageLink
        style={{
          color: "red",
          borderColor: "red",
        }}
        to="/posts/admin"
      >
        Back
      </PageLink>
      <PostForm />
    </>
  );
}
