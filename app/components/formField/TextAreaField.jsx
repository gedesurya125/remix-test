import React from "react";

// External Components
import { Textarea } from "@chakra-ui/react";

// Local Components
import { FieldWrapper } from "./FieldWrapper";

export const TextAreaField = ({ id, name, error, label, value, ...props }) => {
  const [fieldValue, setFieldValue] = React.useState(value);

  return (
    <FieldWrapper htmlFor={id} error={error} label={label}>
      <Textarea
        id={id}
        rows={20}
        name={name}
        {...props}
        value={fieldValue}
        onChange={(e) => setFieldValue(e.target.value)}
      />
    </FieldWrapper>
  );
};
