import React from "react";

// Local components
import { TextField, TextAreaField } from "./formField";

// External Components
import { Form, useActionData } from "@remix-run/react";
import { Button, Box } from "@chakra-ui/react";

export const PostForm = ({ initialValue }) => {
  const errors = useActionData();

  return (
    <Box
      sx={{
        width: "80%",
        mx: "auto",
      }}
    >
      <Form method="post">
        <TextField
          label="Post Title"
          type="text"
          name="title"
          id="title"
          error={errors?.title}
          value={initialValue?.title}
        />
        <TextField
          label="Post Slug"
          type={initialValue ? "hidden" : "text"}
          name="slug"
          id="slug"
          error={errors?.slug}
          value={initialValue?.slug}
        />
        <TextAreaField
          label="Markdown"
          name="markdown"
          id="markdown"
          error={errors?.markdown}
          value={initialValue?.markdown}
        />
        <Button
          type="submit"
          name="intent"
          bg="teal"
          color="white"
          value="submit"
          sx={{
            mt: "1rem",
          }}
        >
          {initialValue ? "Update" : "Create"} Post
        </Button>
        {initialValue && (
          <Button
            type="submit"
            name="intent"
            bg="red"
            color="white"
            sx={{
              mt: "1rem",
              ml: "1rem",
            }}
            value="delete"
          >
            Delete
          </Button>
        )}
      </Form>
    </Box>
  );
};
