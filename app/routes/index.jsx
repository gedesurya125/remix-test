import React from "react";
import { Link } from "@remix-run/react";

// External Components
import { Box } from "@chakra-ui/react";
// Local Components

import { PageLink } from "~/components";

export default function Index() {
  return (
    <div>
      <h1>Welcome to Remix</h1>
      <Box
        as="ul"
        sx={{
          border: "1px solid blue",
          width: "max-content",
          p: "1rem 2rem",
        }}
      >
        <li>
          <a
            target="_blank"
            href="https://remix.run/tutorials/blog"
            rel="noreferrer"
          >
            15m Quickstart Blog Tutorial
          </a>
        </li>
        <li>
          <a
            target="_blank"
            href="https://remix.run/tutorials/jokes"
            rel="noreferrer"
          >
            Deep Dive Jokes App Tutorial
          </a>
        </li>
        <li>
          <a target="_blank" href="https://remix.run/docs" rel="noreferrer">
            Remix
          </a>
        </li>
      </Box>
      <PageLink to="/posts" style={{ marginTop: "1rem" }}>
        Blog Posts
      </PageLink>
    </div>
  );
}
