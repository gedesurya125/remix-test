import { json } from "@remix-run/node";
import { useLoaderData, Link, useActionData } from "@remix-run/react";
import { getPosts } from "~/models/post.server";

// External Components
import { Text, Heading, Box } from "@chakra-ui/react";
// Local Components
import { PageLink } from "~/components";

export const loader = async () => {
  return json({
    posts: await getPosts(),
  });
};

export default function Posts() {
  const { posts } = useLoaderData();
  console.log(posts);
  return (
    <main>
      <Heading
        as="h1"
        sx={{
          color: "primary",
          p: 0,
          fontSize: "4rem",
        }}
      >
        Posts
      </Heading>

      <Box as="nav">
        <Box
          as="ul"
          sx={{
            mt: "1rem",
          }}
        >
          {posts.map((post) => (
            <Box
              as="li"
              key={post.slug}
              sx={{
                mr: "2rem",
                listStyle: "none",
              }}
            >
              <Link
                style={{
                  color: "blue",
                }}
                to={post.slug}
              >
                {post.title}
              </Link>
            </Box>
          ))}
        </Box>
      </Box>
      <PageLink to="admin">Go To Admin Page</PageLink>
    </main>
  );
}
