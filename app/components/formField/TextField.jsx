import React from "react";

// Local Components
import { FieldWrapper } from "./FieldWrapper";

// External Components
import { Input } from "@chakra-ui/react";

export const TextField = ({
  type = "text",
  name,
  id,
  error,
  value,
  label,
  ...props
}) => {
  const [fieldValue, setFieldValue] = React.useState(value);

  return (
    <FieldWrapper error={error} htmlFor={id} label={label}>
      {type === "hidden" && value}
      <Input
        type={type}
        name={name}
        id={id}
        value={fieldValue}
        onChange={(e) => setFieldValue(e.target.value)}
        {...props}
      />
    </FieldWrapper>
  );
};
