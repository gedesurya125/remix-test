// context.tsx
// This configuration required by Chakra UI
import React, { createContext } from "react";

export const ServerStyleContext = createContext(null);
export const ClientStyleContext = createContext(null);
