// Local Components
import { PageLink } from "~/components";

// External Components
import { Box } from "@chakra-ui/react";

export default function AdminIndex() {
  return (
    <Box
      sx={{
        display: "flex",
        flexDirection: "column",
        alignItems: "start",
        gap: "1rem",
      }}
    >
      <PageLink to="new">Create a New Post</PageLink>
      <PageLink
        style={{
          color: "red",
          borderColor: "red",
        }}
        to="/posts"
      >
        Exit
      </PageLink>
    </Box>
  );
}
