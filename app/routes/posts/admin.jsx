import { json } from "@remix-run/node";
import { Link, useLoaderData, Outlet } from "@remix-run/react";

import { getPosts } from "~/models/post.server";

// Local Components
import { Box, Heading } from "@chakra-ui/react";

export const loader = async () => {
  return json({ posts: await getPosts() });
};

export default function PostAdmin() {
  const { posts } = useLoaderData();
  return (
    <div>
      <Heading
        sx={{
          textAlign: "center",
          mt: "4rem",
        }}
      >
        Blog Admin
      </Heading>
      <Box as="nav">
        <Box
          as="ul"
          sx={{
            mt: "1rem",
          }}
        >
          <Heading
            as="h2"
            sx={{
              fontStyle: "2rem",
            }}
          >
            Post List
          </Heading>
          {posts.map((post) => (
            <Box
              as="li"
              key={`${post.slug}`}
              sx={{
                mr: "2rem",
                listStyle: "none",
              }}
            >
              <Link
                style={{
                  color: "blue",
                }}
                to={`${post.slug}`}
              >
                {post.title}
              </Link>
            </Box>
          ))}
        </Box>
      </Box>
      <div>
        <Box
          as="main"
          sx={{
            mt: "2rem",
          }}
        >
          <Outlet />
        </Box>
      </div>
    </div>
  );
}
