import { prisma } from "~/db.server";

// export async function getPosts() {
//   return [
//     {
//       slug: "my-first-post",
//       title: "My First Post",
//     },
//     {
//       slug: "90s-mixtape",
//       title: "A Mixtape I Made Just For You",
//     },
//   ];
// }

export async function getPosts() {
  return prisma.post.findMany();
}

export async function getPost(slug) {
  return prisma.post.findUnique({ where: { slug } });
}

export async function createPost(post) {
  return prisma.post.create({ data: post });
}

export async function updatePost(post) {
  return prisma.post.update({
    where: {
      slug: post.slug,
    },
    data: {
      title: post.title,
      markdown: post.markdown,
    },
  });
}

export async function deletePost(postSlug) {
  return prisma.post.delete({
    where: {
      slug: postSlug,
    },
  });
}
